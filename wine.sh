#!/bin/sh

wget -q "http://deb.playonlinux.com/public.gpg" -O- | apt-key add -
wget http://deb.playonlinux.com/playonlinux_trusty.list -O /etc/apt/sources.list.d/playonlinux.list
apt-get update
apt-get -y -qq install playonlinux

dpkg --add-architecture i386
wget -nc https://dl.winehq.org/wine-builds/Release.key
apt-key add Release.key
apt-add-repository https://dl.winehq.org/wine-builds/ubuntu/
apt-get update
apt-get install -y -qq --install-recommends winehq-stable

cp /usr/share/playonlinux/lib/sources /usr/share/playonlinux/lib/sources.bak
cp /usr/share/playonlinux/python/mainwindow.py /usr/share/playonlinux/python/mainwindow.py.bak

sed -i "19,23d" /usr/share/playonlinux/lib/sources
sed -i "1216,1218d" /usr/share/playonlinux/python/mainwindow.py

wget http://app.pc.kakao.com/talk/win32/KakaoTalk_Setup.exe -O /root/Desktop/KakaoTalk_Setup.exe

rm $0
