# Ubuntu 14.04.5 LTS
Goorm.io Workspace [LXDE] Docker Container

    git clone https://gitlab.com/idonly4project/Goorm-VNC-Container.git; Goorm-VNC-Container/install-lxde.sh; Goorm-VNC-Container/wine.sh

### 01. Applications
---
설치되어 있는 어플리케이션

#### Base Applications
- `fcitx`, `fcitx-hangul`: 한글 키보드 입력용
- `tigervncserver` : VNC 서버 지원용 (기존 `tightvncserver`보다 지원성이 높음)
- `noVNC` : Websocketify를 이용한 웹 브라우저에서 접근할 수 있는 VNC 클라이언트(제공자 서버)
- `LXDE` : Lightweight X-Desktop Environment. 이만큼 가벼운 Desktop Environment도 없다.

#### Works Application
- `Midori` : Firefox, Chrome 및 Chromium 등 Webkit 기반 대체 브라우져. `/dev/shm/`(공유 메모리)가 꽉 차 Crash되는 현상을 방지할 수 있는 Alternative 브라우져이다. 이 브라우져는 Shared memory를 사용하지 않는다.
